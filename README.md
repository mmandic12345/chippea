# ChipPea

ChipPea is a System on Chip (SoC) design written in Verilog specifically for the Cyclone-V FPGA board. It is synthesized using Intel Quartus II and contains simulations for some of its modules.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)


## Introduction

ChipPea is a SoC design aimed at learning and understanding Verilog and FPGA. It explores simulation and synthesis aspects of chip design.

## Features

- **Verilog Implementation**: Entire SoC design is written in Verilog, allowing for easy customization and expansion.
- **Cyclone-V FPGA Compatibility**: Designed specifically for the Cyclone-V FPGA board, ensuring optimal performance and compatibility.
- **Intel Quartus II Synthesis**: Synthesized using Intel Quartus II, a powerful FPGA design software suite.
- **Simulations**: Includes simulation models for some of its modules, facilitating testing and validation of the design.

## Usage

To use ChipPea, follow these steps:

1. **Connect Cyclone-V to PC**: Connect your Cyclone-V FPGA board to your PC using USB cable.
2. **Synthesize and Program FPGA**: Type the following command in the terminal to synthesize the design and program the FPGA:
   ```bash
   make synth_pgm
