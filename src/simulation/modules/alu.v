module alu #(parameter WIDTH = 4)(
    input wire [2:0]oc,
    input wire [WIDTH-1:0]a,
    input wire [WIDTH-1:0]b,
    output wire [WIDTH-1:0]f
);

    assign f =
    (!oc[2] & !oc[1] & !oc[0]) ? (a + b) : (
    (!oc[2] & !oc[1] & oc[0]) ?  (a - b) : (
    (!oc[2] & oc[1] & !oc[0]) ?  (a * b) : (
    (!oc[2] & oc[1]) & oc[0]) ?  (a / b) : (
    (oc[2] & !oc[1] & !oc[0]) ?  (~a)    : (
    (oc[2] & !oc[1] & oc[0]) ?   (a ^ b) : (
    (oc[2] & oc[1] & !oc[0]) ?   (a | b) : (
                                 (a & b)
    ))))));

endmodule
