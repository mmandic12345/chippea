module register #(parameter WIDTH = 4)(
    input clk,
    input rst_n,
    input cl,
    input ld,
    input [WIDTH-1:0]in,
    input inc,
    input dec,
    input sr,
    input ir,
    input sl,
    input il,
    output [WIDTH-1:0]out
);

    reg [WIDTH-1:0]out_next, out_reg;
    assign out = out_reg;


    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            out_reg <= {WIDTH{1'b0}};
        end else begin
            out_reg <= out_next;
        end
    end

    always @(*) begin
        if (cl) begin out_next = {WIDTH{1'b0}}; end
        else if (ld) begin out_next = in; end
        else if (inc) begin out_next = out_reg + 1'b1; end
        else if (dec) begin out_next = out_reg - 1'b1; end
        else if (sr) begin out_next = {ir, out_reg[3:1]}; end
        else if (sl) begin out_next = {out_reg[2:0], il}; end
        else begin out_next = out_reg; end
    end

endmodule
