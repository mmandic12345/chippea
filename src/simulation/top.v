module top();

    parameter WIDTH = 4;
    reg [2:0] oc;
    reg [WIDTH-1:0] a, b;
    wire [WIDTH-1:0] f;

    alu #(.WIDTH(WIDTH)) alu_instance(.oc(oc), .a(a), .b(b), .f(f));


    reg clk;
    reg rst_n;
    reg cl;
    reg ld;
    reg [WIDTH-1:0]in;
    reg inc;
    reg dec;
    reg sr;
    reg ir;
    reg sl;
    reg il;
    wire [WIDTH-1:0]out;

    register register_instance(
        .clk(clk),
        .rst_n(rst_n),
        .cl(cl),
        .ld(ld),
        .in(in),
        .inc(inc),
        .dec(dec),
        .sr(sr),
        .ir(ir),
        .sl(sl),
        .il(il),
        .out(out)
    );

    initial begin
        oc = 3'b000;
        a = 0;
        b = 0;

        repeat (2 ** (3 + WIDTH + WIDTH)) begin
            $strobe("Time = %t, oc = %b, a = %d, b = %d, f = %d", $time, oc, a, b, f);
            #10 {oc, a, b} = {oc, a, b} + 1;
        end

        #10 $stop;

        #7 rst_n = 1'b1;
        repeat (1000) begin
            {cl, ld, in, inc, dec, sr, ir, sl, il} = $urandom_range(4095);
            #10;
        end
        $finish;

    end


    initial begin
        rst_n = 1'b1;
        clk = 1'b0;
        forever 
            #5 clk = ~clk;
    end

    always @(out)
        $strobe(
            "time = %4d, in = %d, out = %d, cl = %d, ld = %d, inc = %d, dec = %d, sr = %d, ir = %d, sl = %d, il= %d",
            $time, in, out, cl, ld, inc, dec, sr, ir, sl, il
        );

endmodule
