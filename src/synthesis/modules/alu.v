module alu #(parameter DATA_WIDTH = 16)(
    input wire [2:0]oc,
    input wire [DATA_WIDTH-1:0]a,
    input wire [DATA_WIDTH-1:0]b,
    output wire [DATA_WIDTH-1:0]f
);

    assign f =
    (!oc[2] & !oc[1] & !oc[0]) ? (a + b) : (
    (!oc[2] & !oc[1] & oc[0]) ?  (a - b) : (
    (!oc[2] & oc[1] & !oc[0]) ?  (a * b) : (
    (!oc[2] & oc[1]) & oc[0]) ?  (a / b) : (
    (oc[2] & !oc[1] & !oc[0]) ?  (~a)    : (
    (oc[2] & !oc[1] & oc[0]) ?   (a ^ b) : (
    (oc[2] & oc[1] & !oc[0]) ?   (a | b) : (
                                 (a & b)
    ))))));

endmodule
