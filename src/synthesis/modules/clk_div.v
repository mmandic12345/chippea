module clk_div #(parameter DIVISOR = 50000000)(
    input clk,
    input rst_n,
    output out
);

    integer count, count_next;
	reg out_reg, out_next;

	assign out = out_reg;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            count <= 32'd0;
            out_reg <= 1'b0;
        end
        else begin
        	out_reg <= out_next;
            count <= count_next;
        end
    end

    always @(*) begin
        out_next = out_reg;
        count_next = count;
        if (count_next == DIVISOR - 1) begin
        	count_next = 32'b0;
        	out_next = ~out_next;
        end else begin
        	count_next = count_next + 1'b1;
        end
    end

endmodule

