module cpu #(
	parameter ADDR_WIDTH = 6,
	parameter DATA_WIDTH = 16
)(
	input clk,
	input rst_n,
	input [DATA_WIDTH-1:0]mem_in,
	input [DATA_WIDTH-1:0]in,
	output mem_we,
	output [ADDR_WIDTH-1:0]mem_addr,
	output [DATA_WIDTH-1:0]mem_data,
	output [DATA_WIDTH-1:0]out,
	output [ADDR_WIDTH-1:0]pc,
	output [ADDR_WIDTH-1:0]sp
);

	localparam INITIAL = 0;
	localparam FETCH_INSTRUCTION = 1;
	localparam DECODE = 2;
	localparam FETCH_CONSTANT = 3;
	localparam FETCH_MEM_REG_ARG1 = 4;
	localparam FETCH_MEM_REG_ARG2 = 5;
	localparam FETCH_MEM_REG_ARG3 = 6;
	localparam EXECUTE = 7;
	localparam WRITE_TO_MEMORY = 8;
	localparam STOP_OUTPUT_1 = 9;
	localparam STOP_OUTPUT_2 = 10;
	localparam STOP_OUTPUT_3 = 11;
	localparam FULL_STOP = 12;
	integer current_state, next_state;

	reg [DATA_WIDTH-1:0]OUTPUT; assign out = OUTPUT;
	reg [DATA_WIDTH-1:0]OUTPUT_NEXT;

	reg WE; assign mem_we = WE;

	reg [ADDR_WIDTH-1:0]MAR; assign mem_addr = MAR;

	reg [DATA_WIDTH-1:0]MDR; assign mem_data = MDR;

	wire LOAD_PC; assign LOAD_PC = (current_state == INITIAL);
	wire INC_PC; assign INC_PC = ((current_state == FETCH_INSTRUCTION) || (current_state == FETCH_CONSTANT));
	wire [DATA_WIDTH-1:0]PC_OUTPUT;
	register #(.DATA_WIDTH(6)) PC(.clk(clk), .rst_n(1'b1), .cl(1'b0), .ld(LOAD_PC), .in(6'b001000), .inc(INC_PC), .dec(1'b0), .sr(1'b0), .ir(1'b0), .sl(1'b0), .il(1'b0), .out(PC_OUTPUT));

	wire LOAD_SP; assign LOAD_SP = (current_state == INITIAL);
	wire INC_SP; assign INC_SP = 1'b0;
	wire DEC_SP; assign DEC_SP = 1'b0;
	wire [5:0]SP_OUTPUT;
	register #(.DATA_WIDTH(6)) SP(.clk(clk), .rst_n(1'b1), .cl(1'b0), .ld(LOAD_SP), .in(6'b111111), .inc(INC_SP), .dec(DEC_SP), .sr(1'b0), .ir(1'b0), .sl(1'b0), .il(1'b0), .out(SP_OUTPUT));
	
	wire CLEAR_IR_UPPER; assign CLEAR_IR_UPPER = 1'b0;
	wire LOAD_IR_UPPER; assign LOAD_IR_UPPER = (current_state == FETCH_INSTRUCTION) || (current_state == DECODE);
	wire [DATA_WIDTH-1:0]IR_UPPER_INPUT; assign IR_UPPER_INPUT = mem_in;
	wire [DATA_WIDTH-1:0]IR_UPPER_OUTPUT;
	wire MOV_CONST; assign MOV_CONST = (IR_UPPER_OUTPUT[15:12] == 4'b0000) && (IR_UPPER_OUTPUT[3:0] == 4'b1000);
	wire MOV; assign MOV = (IR_UPPER_OUTPUT[15:12] == 4'b0000);
	wire IN; assign IN = (IR_UPPER_OUTPUT[15:12] == 4'b0111);
	wire OUT; assign OUT = (IR_UPPER_OUTPUT[15:12] == 4'b1000);
	wire ADD; assign ADD = (IR_UPPER_OUTPUT[15:12] == 4'b0001);
	wire SUB; assign SUB = (IR_UPPER_OUTPUT[15:12] == 4'b0010);
	wire MUL; assign MUL = (IR_UPPER_OUTPUT[15:12] == 4'b0011);
	wire DIV; assign DIV = (IR_UPPER_OUTPUT[15:12] == 4'b0100);
	wire ALUINST; assign ALUINST = ADD || SUB || MUL || DIV;
	wire STOP; assign STOP = (IR_UPPER_OUTPUT[15:12] == 4'b1111);
	wire IND1; assign IND1 = IR_UPPER_OUTPUT[11];
	wire IND2; assign IND2 = IR_UPPER_OUTPUT[7];
	wire IND3; assign IND3 = IR_UPPER_OUTPUT[3];
	wire D1; assign D1 = (~IR_UPPER_OUTPUT[11] && !STOP) || (STOP && IR_UPPER_OUTPUT[10:8] != 3'b000 && ~IR_UPPER_OUTPUT[11]);
	wire D2; assign D2 = (~IR_UPPER_OUTPUT[7] && !STOP) || (STOP && IR_UPPER_OUTPUT[6:4] != 3'b000 && ~IR_UPPER_OUTPUT[7]);
	wire D3; assign D3 = (~IR_UPPER_OUTPUT[3] && !STOP) || (STOP && IR_UPPER_OUTPUT[2:0] != 3'b000 && ~IR_UPPER_OUTPUT[3]);
	wire [2:0]ARG1 = IR_UPPER_OUTPUT[10:8];
	wire [2:0]ARG2 = IR_UPPER_OUTPUT[7:4];
	wire [2:0]ARG3 = IR_UPPER_OUTPUT[3:0];
	register IR_UPPER(.clk(clk), .rst_n(1'b1), .cl(CLEAR_IR_UPPER), .ld(LOAD_IR_UPPER), .in(IR_UPPER_INPUT), .inc(1'b0), .dec(1'b0), .sr(1'b0), .ir(1'b0), .sl(1'b0), .il(1'b0), .out(IR_UPPER_OUTPUT));
	
	wire CLEAR_IR_LOWER; assign CLEAR_IR_LOWER = 1'b0;
	wire LOAD_IR_LOWER; assign LOAD_IR_LOWER = (current_state == FETCH_CONSTANT);
	wire [DATA_WIDTH-1:0]IR_LOWER_INPUT; assign IR_LOWER_INPUT = mem_in;
	wire [DATA_WIDTH-1:0]IR_LOWER_OUTPUT;
	register IR_LOWER(.clk(clk), .rst_n(1'b1), .cl(CLEAR_IR_LOWER), .ld(LOAD_IR_LOWER), .in(IR_LOWER_INPUT), .inc(1'b0), .dec(1'b0), .sr(1'b0), .ir(1'b0), .sl(1'b0), .il(1'b0), .out(IR_LOWER_OUTPUT));
	
	wire [31:0]IR_OUTPUT = {IR_UPPER_OUTPUT, IR_LOWER_OUTPUT};

	wire LOAD_A; assign LOAD_A = (current_state == EXECUTE);
	wire [DATA_WIDTH-1:0]A_OUT;
	register A(.clk(clk), .rst_n(rst_n), .cl(1'b1), .ld(LOAD_A), .in(ALUOUT), .inc(1'b0), .dec(1'b0), .sr(1'b0), .ir(1'b0), .sl(1'b0), .il(1'b0), .out(A_OUT));	

	wire [1:0]STOP_OUTPUT_ARGS; assign STOP_OUTPUT_ARGS = ((IR_UPPER_OUTPUT[11:8] != 4'b0000) && (IR_UPPER_OUTPUT[7:4] != 4'b0000) && (IR_UPPER_OUTPUT[3:0] != 4'b0000)) ? (2'd3) : (((IR_UPPER_OUTPUT[11:8] != 4'b0000) && (IR_UPPER_OUTPUT[7:4] != 4'b0000)) ? (2'd2) : ((IR_UPPER_OUTPUT[11:8] != 4'b0000) ? (2'd1) : (2'd0)));


	reg [DATA_WIDTH-1:0] REG [8];
	reg [DATA_WIDTH-1:0] REG_NEXT [8];

	reg [DATA_WIDTH-1:0]MEM_REG_ARG1, MEM_REG_ARG2, MEM_REG_ARG3;
	reg [DATA_WIDTH-1:0]MEM_REG_ARG1_NEXT, MEM_REG_ARG2_NEXT, MEM_REG_ARG3_NEXT;

	integer CONSTANT;
	integer CONSTANT_NEXT;

	wire [DATA_WIDTH-1:0]VALUE1; assign VALUE1 = (D1) ? (REG[ARG1]) : (MEM_REG_ARG1);
	wire [DATA_WIDTH-1:0]VALUE2; assign VALUE2 = (D2) ? (REG[ARG2]) : (MEM_REG_ARG2);
	wire [DATA_WIDTH-1:0]VALUE3; assign VALUE3 = (D3) ? (REG[ARG3]) : (MEM_REG_ARG3);
	wire [ADDR_WIDTH-1:0]ADDRESS1; assign ADDRESS1 = (D1) ? (ARG1) : (REG[ARG1]);

	wire [2:0]ALUOPERATION; assign ALUOPERATION = (ADD) ? (3'b000) : ((SUB) ? (3'b001) : ((MUL) ? (3'b010) : ((DIV) ? (3'b011) : (3'b100))));
	wire [DATA_WIDTH-1:0]ALUOUT;
	alu ALU(.oc(ALUOPERATION), .a(VALUE2), .b(VALUE3), .f(ALUOUT));

	assign sp = SP_OUTPUT;
	assign pc = PC_OUTPUT;

	always @(posedge clk or negedge rst_n) begin
		if (!rst_n) begin
			current_state <= INITIAL;
			REG[0] <= 0;
			REG[1] <= 0;
			REG[2] <= 0;
			REG[3] <= 0;
			REG[4] <= 0;
			REG[5] <= 0;
			REG[6] <= 0;
			REG[7] <= 0;
			MEM_REG_ARG1 <= 0;
			MEM_REG_ARG2 <= 0;
			MEM_REG_ARG3 <= 0;
			OUTPUT <= 0;
			CONSTANT <= 0;
		end
		else begin
			current_state <= next_state;
			REG[0] <= REG_NEXT[0];
			REG[1] <= REG_NEXT[1];
			REG[2] <= REG_NEXT[2];
			REG[3] <= REG_NEXT[3];
			REG[4] <= REG_NEXT[4];
			REG[5] <= REG_NEXT[5];
			REG[6] <= REG_NEXT[6];
			REG[7] <= REG_NEXT[7];
			MEM_REG_ARG1 <= MEM_REG_ARG1_NEXT;
			MEM_REG_ARG2 <= MEM_REG_ARG2_NEXT;
			MEM_REG_ARG3 <= MEM_REG_ARG3_NEXT;
			OUTPUT <= OUTPUT_NEXT;
			CONSTANT <= CONSTANT_NEXT;
		end
	end


	always @(*) begin
		next_state = current_state;
		REG_NEXT[0] = REG[0];
		REG_NEXT[1] = REG[1];
		REG_NEXT[2] = REG[2];
		REG_NEXT[3] = REG[3];
		REG_NEXT[4] = REG[4];
		REG_NEXT[5] = REG[5];
		REG_NEXT[6] = REG[6];
		REG_NEXT[7] = REG[7];
		MEM_REG_ARG1_NEXT = MEM_REG_ARG1;
		MEM_REG_ARG2_NEXT = MEM_REG_ARG2;
		MEM_REG_ARG3_NEXT = MEM_REG_ARG3;
		CONSTANT_NEXT = CONSTANT;
		OUTPUT_NEXT = OUTPUT;

		WE = 0;
		MAR = PC_OUTPUT;
		case (current_state)
			INITIAL: begin
				MAR = PC_OUTPUT;
				next_state = FETCH_INSTRUCTION;
			end
			WRITE_TO_MEMORY: begin
				MAR = PC_OUTPUT;
				next_state = FETCH_INSTRUCTION;
			end
			FETCH_INSTRUCTION: begin
				MAR = PC_OUTPUT;
				next_state = DECODE;
			end
			DECODE: begin
				if (MOV_CONST) begin 
					MAR = PC_OUTPUT;
					next_state = FETCH_CONSTANT;
				end else if ((OUT && IND1) || (STOP && IND1)) begin
					MAR = REG[ARG1];
					next_state = FETCH_MEM_REG_ARG1;
				end else if ((MOV && IND2) || (ALUINST && IND2)) begin
					MAR = REG[ARG2];
					next_state = FETCH_MEM_REG_ARG2;
				end else if ((ALUINST && D2 && IND3)) begin
					MAR = REG[ARG3];
					next_state = FETCH_MEM_REG_ARG3;
				end else begin
					MAR = PC_OUTPUT;
					next_state = EXECUTE;
				end
			end
			FETCH_CONSTANT: begin
				MAR = PC_OUTPUT;
				CONSTANT_NEXT = mem_in;
				next_state = EXECUTE; // fc, e
			end
			FETCH_MEM_REG_ARG1: begin
				MEM_REG_ARG1_NEXT = mem_in;
				if (STOP && IND2) begin
					MAR = REG[ARG2];
					next_state = FETCH_MEM_REG_ARG2;
				end else if (STOP && IND3) begin
					MAR = REG[ARG3];
					next_state = FETCH_MEM_REG_ARG3;
				end else begin
					MAR = PC_OUTPUT;
					next_state = EXECUTE;
				end
			end
			FETCH_MEM_REG_ARG2: begin
				MEM_REG_ARG2_NEXT = mem_in;
				if ((ALUINST || STOP) && IND3) begin
					MAR = REG[ARG2];
					next_state = FETCH_MEM_REG_ARG3;
				end else begin
					next_state = EXECUTE;
				end
			end
			FETCH_MEM_REG_ARG3: begin
				MEM_REG_ARG3_NEXT = mem_in;
				next_state = EXECUTE;
			end
			EXECUTE: begin
				next_state = WRITE_TO_MEMORY;
				if (STOP) begin 
					if (STOP_OUTPUT_ARGS == 0) begin 
						next_state = FULL_STOP; 
					end else begin
						next_state = STOP_OUTPUT_1;
						OUTPUT_NEXT = VALUE1;
					end
				end else if (MOV_CONST) begin
					REG_NEXT[ARG1] = (D1) ? (CONSTANT) : (REG_NEXT[ARG1]);
					MDR = CONSTANT;
					MAR = ADDRESS1;
					WE = 1;
					next_state = WRITE_TO_MEMORY;
				end else if (MOV) begin 
					REG_NEXT[ARG1] = (D1) ? (VALUE2) : (REG_NEXT[ARG1]);
					MDR = VALUE2;
					MAR = ADDRESS1;
					WE = 1;
					next_state = WRITE_TO_MEMORY;
				end else if (IN) begin
					REG_NEXT[ARG1] = (D1) ? (in) : (REG_NEXT[ARG1]);
					MDR = in;
					MAR = ADDRESS1;
					WE = 1;
					next_state = WRITE_TO_MEMORY;
				end else if (OUT) begin
					OUTPUT_NEXT = (D1) ? (REG[ARG1]) : (MEM_REG_ARG1);
					next_state = FETCH_INSTRUCTION;
				end else if (ALUINST && !DIV) begin
					REG_NEXT[ARG1] = (D1) ? (ALUOUT) : (REG_NEXT[ARG1]);
					MDR = ALUOUT;
					MAR = ADDRESS1;
					WE = 1;
					next_state = WRITE_TO_MEMORY;
				end else begin
					next_state = WRITE_TO_MEMORY;
				end
			end
			STOP_OUTPUT_1: begin
				if (STOP_OUTPUT_ARGS == 1) begin 
					next_state = FULL_STOP; 
				end else begin
					next_state = STOP_OUTPUT_2;
					OUTPUT_NEXT = VALUE2; 
				end
			end
			STOP_OUTPUT_2: begin
				if (STOP_OUTPUT_ARGS == 2) begin 
					next_state = FULL_STOP; 
				end else begin 
					next_state = STOP_OUTPUT_3;
					OUTPUT_NEXT = VALUE3;
				end
			end
			STOP_OUTPUT_3: begin
				OUTPUT_NEXT = 0;
				next_state = FULL_STOP;
			end
			FULL_STOP: begin
				next_state = FULL_STOP;
			end
			default: begin 
				next_state = FETCH_INSTRUCTION; // Default state
			end
		endcase
	end

endmodule

/*
MOV R1, 0xFFFF				
MOV &R1, 0xFFFF				f, d, fc, e, wm
MOV R1, R2					f, d, e, wm
MOV R1, &R2					f, d, fmemreg2, e, wm
MOV &R1, R2					f, d, e, wm
MOV &R1, &R2				f, d, fmemreg2, e, wm
IN R1						f, d, e, wm
IN &R1						f, d, e, wm
OUT R1,						f, d, e
OUT &R1						f, d, fmemreg1, e
ALU R1, R2, R3				f, d, e, wm
ALU R1, R2, &R3				f, d, fmemreg3, e, wm
ALU R1, &R2, R3				f, d, fmemreg2, e, wm
ALU R1, &R2, &R3			f, d, fmemreg2, fmemreg3, e, wm
ALU &R1, R2, R3				f, d, e, wm
ALU &R1, R2, &R3			f, d, fmemreg3, e, wm
ALU &R1, &R2, R3			f, d, fmemreg2, e, wm
ALU &R1, &R2, &R3			f, d, fmemreg2, fmemreg3, e, wm
STOP						f, d, e, fs
STOP R1						f, d, e, so1, fs
STOP &R1					f, d, fmemreg1, e, so1, fs
STOP R1, R2					f, d, e, so1, so2, fs
STOP R1, &R2				f, d, fmemreg2, e, so1, so2, fs
STOP &R1, R2				f, d, fmemreg1, e, so1, so2, fs
STOP &R1, &R2				f, d, fmemreg1, fmemreg2, e, so1, so2, fs
STOP R1, R2, R3				f, d, e, so1, so2, so3, fs
STOP R1, R2, &R3			f, d, fmemreg3, e, so1, so2, so3, fs
STOP R1, &R2, R3			f, d, fmemreg2, e, so1, so2, so3, fs
STOP R1, &R2, &R3			f, d, fmemreg2, fmemreg3, e, so1, so2, so3, fs
STOP &R1, R2, R3			f, d, fmemreg1, e, so1, so2, so3, fs
STOP &R1, R2, &R3			f, d, fmemreg1, fmemreg3, e, so1, so2, so3, fs
STOP &R1, &R2, R3			f, d, fmemreg1, fmemreg2, e, so1, so2, so3, fs
STOP &R1, &R2, &R3			f, d, fmemreg1, fmemreg2, fmemreg3, e, so1, so2, so3, fs
*/

