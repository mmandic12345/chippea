module top#(
    parameter DIVISOR = 50000000,
    parameter FILE_NAME = "mem_init.mif",
    parameter ADDR_WIDTH = 6,
    parameter DATA_WIDTH = 16
)(
    input clk,
    input rst_n,
    input [2:0]btn,
    input [9:0]sw,
    output [9:0]led,
    output [27:0]hex
);

    wire CLK_DIV_OUT;
    clk_div #(.DIVISOR(DIVISOR)) CLK_DIV0(.clk(clk), .rst_n(sw[9]), .out(CLK_DIV_OUT));
    
    wire [DATA_WIDTH-1:0]MEM_OUT;
    wire MEM_WE;
    wire [ADDR_WIDTH-1:0]MEM_ADDR;
    wire [DATA_WIDTH-1:0]MEM_DATA;
    wire [ADDR_WIDTH-1:0]SP;
    wire [ADDR_WIDTH-1:0]PC;
    memory #(.FILE_NAME(FILE_NAME), .ADDR_WIDTH(ADDR_WIDTH), .DATA_WIDTH(DATA_WIDTH)) MEMORY_INST0(.clk(CLK_DIV_OUT), .addr(MEM_ADDR), .data(MEM_DATA), .out(MEM_OUT));
    cpu #(.ADDR_WIDTH(ADDR_WIDTH), .DATA_WIDTH(DATA_WIDTH)) CPU0(.clk(CLK_DIV_OUT), .rst_n(sw[9]), .in(sw[3:0]), .mem_we(MEM_WE), .mem_addr(MEM_ADDR), .mem_data(MEM_DATA), .mem_in(MEM_OUT), .pc(PC), .out(led[4:0]), .sp(SP));

    wire [3:0]BCD0_TENS;
    wire [3:0]BCD0_ONES;
    bcd BCD0(.in(SP), .tens(BCD0_TENS), .ones(BCD0_ONES));
    wire [3:0]BCD1_TENS;
    wire [3:0]BCD1_ONES;
    bcd BCD1(.in(PC), .tens(BCD1_TENS), .ones(BCD1_ONES));

    ssd SSD0(.in(BCD0_TENS), .out(hex[27:21]));
    ssd SSD1(.in(BCD0_ONES), .out(hex[20:14]));
    ssd SSD2(.in(BCD1_TENS), .out(hex[13:7]));
    ssd SSD3(.in(BCD1_ONES), .out(hex[6:0]));

endmodule
